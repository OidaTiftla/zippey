﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace zippey {

    public class Program {
        private static readonly Encoding ENCODING = Encoding.UTF8;
        private static string[] TEXT_EXTENSIONS = new string[] { ".txt", ".html", ".xml", ".eo" };
        private static string NEW_LINE = "\r\n";
        private static string NEW_LINE_FOR_LENGTH = "\n";

        public static void Main(string[] args) {
            try {
                using (var input = Console.OpenStandardInput())
                using (var output = Console.OpenStandardOutput()) {
                    if (args.Length < 1 || args[0].FirstOrDefault() == '-' || args[0] == "--help") {
                        Console.Error.WriteLine(AppDomain.CurrentDomain.FriendlyName);
                        Console.Error.WriteLine("To encode: 'zippey.exe e'");
                        Console.Error.WriteLine("To encode: 'zippey.exe ei' (ignore line endings -> always use LF) [compatibility mode with the python version]");
                        Console.Error.WriteLine("To decode: 'zippey.exe d'");
                        Console.Error.WriteLine("All files read from stdin and printed to stdout");
                        Environment.Exit(-1);
                    } else if (args[0].Trim().ToLower() == "e")
                        encode(input, output);
                    else if (args[0].Trim().ToLower() == "ei")
                        encode(input, output, true);
                    else if (args[0].Trim().ToLower() == "d")
                        decode(input, output);
                    else {
                        Console.Error.WriteLine($"Illegal argument '{args[1]}'. Try --help for more information");
                        Environment.Exit(-1);
                    }
                }
            } catch (Exception ex) {
                Console.Error.WriteLine("Exception occurred:");
                Console.Error.WriteLine(ex.ToString());
                Environment.Exit(-1);
            }
        }

        private static Regex regexNewLines_ = new Regex(@"\r\n|\r|\n");

        /// <summary>
        /// Encode into special VCS friendly format from input to output
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public static void encode(Stream input, Stream output, bool ignoreLineEndings = false) {
            using (var writer = new StreamWriter(output, ENCODING, 1024, true))
            using (var memInput = new MemoryStream()) {
                using (var bwriter = new BinaryWriter(memInput, ENCODING, true)) {
                    var buffer = new byte[1024];
                    var len = 0;
                    do {
                        len = input.Read(buffer, 0, 1024);
                        bwriter.Write(buffer, 0, len);
                    } while (len >= 1024);
                }
                memInput.Position = 0;

                using (var reader = new StreamReader(memInput, ENCODING, false, 1024, true)) {
                    var content = reader.ReadToEnd();
                    if (!content.StartsWith("PK\u0003\u0004")) {
                        writer.Write(content);
                        return;
                    }
                }

                memInput.Position = 0;
                using (var archive = new ZipArchive(memInput, ZipArchiveMode.Read, true)) {
                    writer.NewLine = NEW_LINE;

                    foreach (var entry in archive.Entries) {
                        byte[] data = null;
                        using (var stream = entry.Open())
                        using (var reader = new BinaryReader(stream)) {
                            data = reader.ReadBytes((int)entry.Length);
                        }
                        var extension = Path.GetExtension(entry.FullName).ToLower();
                        try {
                            // Check if text data
                            var strdataOriginal = ENCODING.GetString(data);
                            var strdata = regexNewLines_.Replace(strdataOriginal, NEW_LINE);
                            if (!TEXT_EXTENSIONS.Contains(extension) && !strdata.All(c => !char.IsControl(c) || Regex.IsMatch(c.ToString(), @"\s")))
                                throw new Exception("Non printable chars detected.");

                            // new lines
                            bool mixedLineEndings;
                            var lineEnding = GetCommonNewLine(strdataOriginal, out mixedLineEndings)
                                .Replace("\r", "CR")
                                .Replace("\n", "LF");
                            if (mixedLineEndings)
                                Console.Error.WriteLine($"File: '{entry.FullName}' has mixed line endings!");

                            // Encode
                            if (ignoreLineEndings)
                                writer.WriteLine($"{GetLength(strdata)}|{data.Length}|A|{entry.FullName}");
                            else
                                writer.WriteLine($"{GetLength(strdata)}|{data.Length}|A|{lineEnding}|{entry.FullName}");
                            writer.WriteLine(strdata);
                            // \r\n to separation from next meta line
                        } catch (Exception) {
                            // Binary data
                            var base64data = Convert.ToBase64String(data);
                            if (ignoreLineEndings)
                                writer.WriteLine($"{base64data.Length}|{data.Length}|B|{entry.FullName}");
                            else
                                writer.WriteLine($"{base64data.Length}|{data.Length}|B|BIN|{entry.FullName}");
                            writer.WriteLine(base64data);
                            // \r\n to separation from next meta line
                        }
                    }
                }
            }
        }

        private static string GetCommonNewLine(string strdata, out bool mixedLineEndings) {
            var matches = regexNewLines_.Matches(strdata);
            var statistics = matches
                .OfType<Match>()
                .Select(x => x.Value)
                .GroupBy(x => x)
                .ToDictionary(x => x.Key, x => x.Count());
            var most = statistics
                .OrderByDescending(x => x.Value)
                .Select(x => x.Key)
                .FirstOrDefault();
            mixedLineEndings = statistics.Count > 1;
            return most;
        }

        /// <summary>
        /// count each new line only with \n
        /// (for compatibility reasons)
        /// </summary>
        /// <param name="strdata"></param>
        /// <returns></returns>
        private static int GetLength(string strdata) {
            return regexNewLines_.Replace(strdata, NEW_LINE_FOR_LENGTH).Length;
        }

        /// <summary>
        /// Decode from special VCS friendly format from input to output
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public static void decode(Stream input, Stream output) {
            var isZipFile = false;
            using (var memInput = new MemoryStream()) {
                using (var bwriter = new BinaryWriter(memInput, ENCODING, true)) {
                    var buffer = new byte[1024];
                    var len = 0;
                    do {
                        len = input.Read(buffer, 0, 1024);
                        bwriter.Write(buffer, 0, len);
                    } while (len >= 1024);
                }
                memInput.Position = 0;

                using (var reader = new StreamReader(memInput, ENCODING, false, 1024, true))
                using (var memArchive = new MemoryStream()) {
                    using (var archive = new ZipArchive(memArchive, ZipArchiveMode.Create, true)) {
                        string remaining = null;
                        while (true) {
                            var meta = remaining + reader.ReadLine();
                            remaining = null;
                            if (string.IsNullOrWhiteSpace(meta))
                                break;
                            if (meta.StartsWith("PK\u0003\u0004")) { // is already zipped
                                isZipFile = true;
                                break;
                            }

                            var metaParts = meta.Split('|');
                            if (metaParts.Length != 5
                                && metaParts.Length != 4)
                                throw new Exception($"Illegal meta data: {meta}");

                            int data_len, raw_len;
                            if (!int.TryParse(metaParts[0], out data_len) || !int.TryParse(metaParts[1], out raw_len))
                                throw new Exception($"Illegal meta data: {meta}");
                            var mode = metaParts[2];
                            var lineEnding = (metaParts.Length == 4 ? "LF" : metaParts[3])
                                .ToUpper()
                                .Replace("CR", "\r")
                                .Replace("LF", "\n");
                            var name = metaParts.Length == 4 ? metaParts[3] : metaParts[4];

                            if (mode == "A") {
                                var len = 0;
                                var sb = new StringBuilder();
                                while (len < data_len) {
                                    var line = reader.ReadLine() + lineEnding;
                                    len += GetLength(line);
                                    sb.Append(line);
                                }
                                var strdata = sb.ToString();
                                if (len > data_len)
                                    strdata = strdata.Substring(0, strdata.Length - lineEnding.Length);
                                else {
                                    var buffer = NEW_LINE.ToArray();
                                    reader.ReadBlock(buffer, 0, NEW_LINE.Length);
                                    if (new string(buffer) != NEW_LINE)
                                        remaining = new string(buffer);
                                }
                                var chars = strdata.ToArray();

                                var entry = archive.CreateEntry(name);
                                using (var stream = entry.Open())
                                using (var writer = new BinaryWriter(stream, ENCODING)) {
                                    writer.Write(chars);
                                }
                            } else if (mode == "B") {
                                var chars = reader.ReadLine().ToArray();
                                byte[] bytes = Convert.FromBase64CharArray(chars, 0, data_len);

                                var entry = archive.CreateEntry(name);
                                using (var stream = entry.Open())
                                using (var writer = new BinaryWriter(stream)) {
                                    writer.Write(bytes);
                                }
                            } else {
                                // Should never reach here
                                throw new Exception($"Illegal mode '{mode}'");
                            }
                        }
                    }

                    if (!isZipFile) {
                        memArchive.Position = 0;
                        using (var breader = new BinaryReader(memArchive, ENCODING, true))
                        using (var bwriter = new BinaryWriter(output, ENCODING, true))
                            bwriter.Write(breader.ReadBytes((int)memArchive.Length));
                    }
                }

                if (isZipFile) {
                    memInput.Position = 0;
                    using (var breader = new BinaryReader(memInput, ENCODING, true))
                    using (var bwriter = new BinaryWriter(output, ENCODING, true))
                        bwriter.Write(breader.ReadBytes((int)memInput.Length));
                    return;
                }
            }
        }
    }
}