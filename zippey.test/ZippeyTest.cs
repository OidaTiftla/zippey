﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace zippey.test {

    [TestClass]
    public class ZippeyTest {

        [TestMethod]
        public void EncodeDecodeTextOnlyTest() {
            var file = @"TestData/model.zip";

            Test(file);
        }

        [TestMethod]
        public void EncodeDecodeBinaryAndTextTest() {
            var file = @"TestData/modelBin.zip";

            Test(file);
        }

        [TestMethod]
        public void EncodeDecodeWordTest() {
            var file = @"TestData/winword.docx";

            Test(file);
        }

        [TestMethod]
        public void EncodeDecodeTextOnlyPythonCompatibleTest() {
            var file = @"TestData/model.zip";

            Test(file, true);
        }

        [TestMethod]
        public void EncodeDecodeBinaryAndTextPythonCompatibleTest() {
            var file = @"TestData/modelBin.zip";

            Test(file, true);
        }

        [TestMethod]
        public void EncodeDecodeWordPythonCompatibleTest() {
            var file = @"TestData/winword.docx";

            Test(file, true);
        }

        [TestMethod]
        public void DecodeZipTest() {
            var file = @"TestData/modelBin.zip";

            Dictionary<string, byte[]> expected = null;
            Dictionary<string, byte[]> result = null;

            using (var stream = File.OpenRead(file))
                expected = GetZipContent(stream);

            using (var input = File.OpenRead(file)) {
                using (var memResult = new MemoryStream()) {
                    Program.decode(input, memResult);

                    memResult.Position = 0;
                    result = GetZipContent(memResult);
                }
            }

            AssertAreEqual(expected, result);
        }

        [TestMethod]
        public void EncodeTextTest() {
            var file = @"TestData/modelBin.zip";

            string expected = null;
            string result = null;

            using (var mem = new MemoryStream()) {
                using (var input = File.OpenRead(file))
                    Program.encode(input, mem);
                mem.Position = 0;
                using (var reader = new StreamReader(mem, Encoding.UTF8, false, 1024, true))
                    expected = reader.ReadToEnd();

                mem.Position = 0;
                using (var memResult = new MemoryStream()) {
                    Program.encode(mem, memResult);

                    memResult.Position = 0;
                    using (var reader = new StreamReader(memResult, Encoding.UTF8, false, 1024, true))
                        result = reader.ReadToEnd();
                }
            }

            Assert.AreEqual(expected, result);
        }

        private static void Test(string file, bool ignoreLineEndings = false) {
            Dictionary<string, byte[]> expected = null;
            Dictionary<string, byte[]> result = null;

            using (var stream = File.OpenRead(file))
                expected = GetZipContent(stream);
            if (ignoreLineEndings)
                IgnoreLineEndings(expected);

            using (var mem = new MemoryStream()) {
                using (var input = File.OpenRead(file))
                    Program.encode(input, mem, ignoreLineEndings);
                mem.Position = 0;
                using (var memResult = new MemoryStream()) {
                    Program.decode(mem, memResult);

                    memResult.Position = 0;
                    result = GetZipContent(memResult);
                }
            }

            AssertAreEqual(expected, result);
        }

        private static void IgnoreLineEndings(Dictionary<string, byte[]> expected) {
            foreach (var key in expected.Keys.ToList()) {
                var data = expected[key];
                try {
                    var strdata = Encoding.UTF8.GetString(data);
                    var extension = Path.GetExtension(key).ToLower();
                    var regex = new Regex(@"\r\n|\r");
                    if (regex.IsMatch(strdata)
                        && (new string[] { ".txt", ".html", ".xml", ".eo" }.Contains(extension)
                        || strdata.All(c => !char.IsControl(c))))
                        expected[key] = Encoding.UTF8.GetBytes(regex.Replace(strdata, "\n"));
                } catch { }
            }
        }

        private static void AssertAreEqual(Dictionary<string, byte[]> expected, Dictionary<string, byte[]> result) {
            CollectionAssert.AreEqual(expected.Keys, result.Keys);
            foreach (var key in expected.Keys)
                CollectionAssert.AreEqual(expected[key], result[key]);
        }

        private static Dictionary<string, byte[]> GetZipContent(Stream stream) {
            var content = new Dictionary<string, byte[]>();
            using (var archive = new ZipArchive(stream, ZipArchiveMode.Read, true)) {
                foreach (var entry in archive.Entries)
                    using (var reader = new BinaryReader(entry.Open()))
                        content[entry.FullName] = reader.ReadBytes((int)entry.Length);
            }

            return content;
        }
    }
}